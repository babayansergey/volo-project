import { Component, OnInit} from '@angular/core';
import { AuthService } from 'shared/services/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  loginForm: FormGroup;
apiUrl:string;


  constructor(private authService: AuthService, private router: Router) { }
  submitForm(user) {
    this.authService.login({ "email": this.loginForm.get('email').value, "password": this.loginForm.get('password').value })
      .subscribe((data) => { this.router.navigate(['/dashboard/userList/1']) }, 
      (error: any) => alert(error));

  }

  logout() {
    this.authService.logOut();
  }
  goToDashboard(){
    this.router.navigate(['/dashboard/userList/1']);
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl("asdg@asgda.ru"),
      password: new FormControl("Aaasdhasdkg1323")
    });
   
  }
}
