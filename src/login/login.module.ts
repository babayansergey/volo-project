import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { LoginRoutingModule } from './login.routing'

import { LoginComponent } from './components/login-component/login.component';
import { EmailValidationDirective } from 'shared/directives/validation.directive';
import { PasswordValidateDirective } from 'shared/directives/password-validate.directive';

import { UserService } from 'shared/services/user.service';

import { ReactiveFormsModule } from "@angular/forms";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpModule,
    LoginRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [LoginComponent,
    PasswordValidateDirective,
    EmailValidationDirective],
  exports: [LoginComponent],
  providers: [
    { provide: 'ApiUrl', useValue: 'https://reqres.in/api/' },
    UserService
  ]

})
export class LoginModule {

}
