import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

  transform(value: string, args?: any): string {
    let ind = 0;
    let arr = value.split(' ');
    arr.forEach((item, index, arr) => {
      if (item) {
        arr[index] = arr[index][0].toUpperCase() + item.slice(1);
      }
    })
    return arr.join(' ');
  }

}
