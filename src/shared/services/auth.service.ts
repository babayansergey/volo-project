import { Injectable,Inject } from '@angular/core';
import { Http } from '@angular/http';
import {Response, Headers} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {

  token:string;
  loginEmail:string;
  apiUrl:string;
  constructor(private http: Http, @Inject('ApiUrl') apiUrl ) { 
     this.token=localStorage.getItem('token');
    this.loginEmail=localStorage.getItem('loginEmail');
    this.apiUrl=apiUrl;
   }

  login(obj) {
    const body = JSON.stringify(obj);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    
    return this.http.post(this.apiUrl+'login',obj,{ headers: headers })
                          .map((resp:Response)=>{
                            this.token=resp.json().token;
                           this.loginEmail=obj.email;
                            localStorage.setItem('token', this.token);
                            localStorage.setItem('loginEmail', this.loginEmail);
                           
                            return resp.json()
                          })
                          .catch((error:any) =>{return Observable.throw(error);}); 
    
  }

  logOut(){
    this.token = ""; 
    localStorage.removeItem('token');
  }
}
