import { Injectable,Inject } from '@angular/core';
import { Http } from '@angular/http';
import { Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { User } from 'shared/user';


@Injectable()
export class UserService {
  apiUrl:string;
  constructor(private http: Http,@Inject('ApiUrl') apiUrl) {
    this.apiUrl=apiUrl;
  }

  //-----------------getUsers & singleUser----------------//
  getUsers(page: number) {
    localStorage.setItem('CurrentPage',page.toString())

    return this.http.get(this.apiUrl+'users?page=' + page)
      .map((resp: Response) => {
        return resp.json()
      })
      .catch((error: any) => { return Observable.throw(error); });

  }

  getSingleUser(id: number) {

    return this.http.get(this.apiUrl+'users/' + id)
      .map((resp: Response) => {
        return resp.json().data;
      })
      .catch((error: any) => { return Observable.throw(error); });

  }

  //-----------------Add,Edit,Delete user----------------//

  addNewUser(newUser) {

    const body = JSON.stringify(newUser);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.post(this.apiUrl+'users', newUser, { headers: headers })
      .map((resp: Response) => {
        return resp.json();
      })
      .catch((error: any) => { return Observable.throw(error); });

  }

  updateUser(user: User) {
    const body = JSON.stringify(user);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.put(this.apiUrl+'users', user, { headers: headers })
      .map((resp: Response) => {
        return resp.json();
      })
      .catch((error: any) => { return Observable.throw(error); });
  }


  deleteUser(user: User) {
    const body = JSON.stringify(user);
    let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
    return this.http.put(this.apiUrl+'users/' + user.id, user, { headers: headers })
      .map((resp: Response) => {
        return resp.json();
      })
      .catch((error: any) => { return Observable.throw(error); });
  }
 

  getCurrentPage(){
    return +localStorage.getItem('CurrentPage');
  }
}

