import { Directive} from '@angular/core';
import { AbstractControl, Validator, NG_VALIDATORS, ValidationErrors } from "@angular/forms";

@Directive({
    selector: '[validatePassword]',
    providers: [{provide: NG_VALIDATORS, useExisting: PasswordValidateDirective, multi: true}]
  })
  export class PasswordValidateDirective implements Validator {
     
    validate(control: AbstractControl):ValidationErrors {
      let regExp:RegExp=/^(?=.*[0-9@#$%^&+=_\-])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    
      return regExp.test(control.value)?null:{'validatePassword': {value: control.value}};
    }
  }