
import { Directive} from '@angular/core';
import { AbstractControl, Validator, NG_VALIDATORS, ValidationErrors } from "@angular/forms";

@Directive({
    selector: '[validateEmail]',
    providers: [{provide: NG_VALIDATORS, useExisting: EmailValidationDirective, multi: true}]
  })
  export class EmailValidationDirective implements Validator {
    
    
    
    
    validate(control: AbstractControl):ValidationErrors {
    let regExp=/^(\w+@[\w+_]+?\.[a-zA-Z]{2,6})$/;
      return regExp.test(control.value)?null:{'validateEmail': {value: control.value}};
    }
  }

