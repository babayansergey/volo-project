import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LoginModule } from 'login/login.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { UserListComponent } from './dashboard/user-list/user-list.component';

import { AuthService } from 'shared/services/auth.service';
import { AuthGuardService } from 'shared/services/auth-guard.service';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { EditUserComponent } from './dashboard/edit-user/edit-user.component';
import { AddUserComponent } from './dashboard/add-user/add-user.component';
import { DeleteConfirmComponent } from './dashboard/delete-confirm/delete-confirm.component';
import { CapitalizePipe } from 'shared/pipes/capitalize.pipe';
import { PaginationComponent } from './pagination/pagination.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    NotFoundComponent,
    UserListComponent,
    EditUserComponent,
    AddUserComponent,
    DeleteConfirmComponent,
    CapitalizePipe,
    PaginationComponent,
    

  ],
  imports: [
    BrowserModule,
    LoginModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule 
  ],
  providers: [
    {provide:'ApiUrl',useValue:'https://reqres.in/api/'},
    AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }