import { Component, OnInit } from '@angular/core';
import { UserService } from 'shared/services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from 'shared/user';


@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  private editingUser: User;

  constructor(private activatedRoute: ActivatedRoute, private userService: UserService, private router: Router) { }

  ngOnInit() {

    this.activatedRoute.params.forEach((params: Params) => {
      this.editingUser = new User();
      let id = +params["index"]; // number
      this.userService.getSingleUser(id).subscribe((data) => {
        Object.assign(this.editingUser, data);
      })

    });
  }
  back() {
    let curPage = localStorage.getItem('CurrentPage');
    this.router.navigate(['/dashboard/userList', curPage]);

  }
  save() {
    this.userService.updateUser(this.editingUser).subscribe(data => {
      
      alert('User successfully edited');
      this.back();
    },
      error => alert(error) + '\nCannot edit this user\nPlease check connection');

  }
}
