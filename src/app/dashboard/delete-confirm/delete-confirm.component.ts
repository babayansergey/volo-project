import { Component, OnInit, Input, Output,EventEmitter} from '@angular/core';
import { User } from 'shared/user';
import { UserService } from 'shared/services/user.service';

@Component({
  selector: 'app-delete-confirm',
  templateUrl: './delete-confirm.component.html',
  styleUrls: ['./delete-confirm.component.css']
})
export class DeleteConfirmComponent implements OnInit {

  @Output() isDeleted:EventEmitter<boolean>=new EventEmitter<boolean>();
  @Input() user:User;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.user= new User();
  }
  confirmDeleting(){
    this.userService.deleteUser(this.user).subscribe(resp=>{
        alert('User has been deleted!');
        this.isDeleted.emit(true);
    },
  error=>alert(error)+'\nCan not delete user!\nPlease check connection')
    
  }

}
