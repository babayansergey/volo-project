import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { User } from 'shared/user';
import { UserService } from 'shared/services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  newUser: User;


  @Output() isSaved: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() id: number;
  constructor(private userService: UserService) {

  }

  ngOnInit() {
    this.newUser = new User();
  }


  save() {

    if (!this.newUser.avatar || !this.newUser.first_name || !this.newUser.last_name) {
      return
    }
    this.userService.addNewUser(this.newUser).subscribe(resp => {
        this.isSaved.emit(true);
    },
      error => alert(error+'\nCan not add a new user\nPlease check connection '));
    
  }






}
