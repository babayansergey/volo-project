import { Component, OnInit } from '@angular/core';
import { UserService } from 'shared/services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from 'shared/user';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  userList: User[] = [];
  sortedColumn: string = "";
  deletingUser: User;
  sub: any; //for subscribe to url change 
  total_pages:number;
  sortByAscending: object = {
    id: 1,
    first_name: 1,
    last_name: 1,
  };

  constructor(private userService: UserService, private router: Router,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.sub = this.activatedRoute.params.subscribe((params: Params) => {
      localStorage.setItem('CurrentPage',params["pageNumber"]);

      this.getUsers()
    });

  }
  //---------------------GetUsers------------//

  getUsers() {
    let curPage=localStorage.getItem('CurrentPage');
    this.userService.getUsers(+curPage).subscribe((resp) => {
      this.total_pages=resp.total_pages;
      this.userList = resp.data;
    }, error => alert(error + '\nCannot get users\nPlease check connection'))
  }

  //---------------Delete,Edit,Add-------------------//
  deleteUser() {
    
    this.getUsers();

  }

  SelectdeletingUser(curUser) {
    this.deletingUser = curUser;

  }

  editUser(curUser) {
    let index = curUser.id;
    if (index == -1) {
      alert('This user already deleted');
    }
    this.router.navigate(['/dashboard/editUser', index]);

  }
  addUser(isSaved) {
    if (!isSaved) {
      return;
    };
    alert('User successfully added');
    this.getUsers();
  }
  //---------------sorting---------------//
  sortByColumn(columnName: string) {


    if (columnName != 'id' && columnName != 'first_name' && columnName != 'last_name') {
      return
    };


    if (this.sortedColumn == columnName) { // on double click change sort direction
      this.sortByAscending[columnName] *= -1;

    }

    this.sortedColumn = columnName;
    let compareBinded = compare.bind(this);
    this.userList.sort(compareBinded);

  }
}
function compare(a, b) {

  if (a[this.sortedColumn] < b[this.sortedColumn])
  { return -1 * this.sortByAscending[this.sortedColumn]; }
  if (a[this.sortedColumn] > b[this.sortedColumn])
  { return 1 * this.sortByAscending[this.sortedColumn]; }
  return 0;
}

