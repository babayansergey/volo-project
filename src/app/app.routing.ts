import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router'
import { NotFoundComponent } from './not-found/not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserListComponent } from './dashboard/user-list/user-list.component';
import { AuthGuardService } from 'shared/services/auth-guard.service';
import { EditUserComponent } from './dashboard/edit-user/edit-user.component';
import { AddUserComponent } from './dashboard/add-user/add-user.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot([
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      {
        path: 'dashboard',
        component: DashboardComponent, 
        canActivate:[AuthGuardService],
        children: [     
          {path:'',pathMatch:'full',redirectTo:'userList/1'},
          { path: 'userList/:pageNumber', component: UserListComponent },
          { path: 'editUser/:index', component: EditUserComponent },
          { path: 'addUser', component: AddUserComponent },
        ]
      },
      
      { path: '**', component: NotFoundComponent },
    ])
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }
