import { Component, OnInit,Input } from '@angular/core';
import { UserService } from 'shared/services/user.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {
  
  @Input() total_pages:number;
  pages:number[]=[];

  constructor(private userService:UserService,private router:Router) {
  }

  ngOnInit() {
    this.createPages(this.total_pages);
  }

  createPages(numOfPages) {
    this.pages = [];
    while (numOfPages) {
      this.pages.unshift(numOfPages);
      numOfPages--;
    }
  }
   changePage(pageNumber) {

    this.router.navigate(['/dashboard/userList', pageNumber]);

  }

}
